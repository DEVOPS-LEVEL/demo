from py2neo import Graph, Node, Relationship
import random
import time
import os

def lambda_handler(event, context):
    # Connect to Neo4j - replace credentials and host with your own
    graph = Graph("bolt://ec2-3-83-254-136.compute-1.amazonaws.com:7687", auth=("neo4j", "A123456a"))

    # Create a unique identifier for each run based on timestamp and random number
    unique_id = int(time.time())

    # Create 100 user nodes with unique identifiers
    users = []
    for i in range(1, 21):
        email = f"user{i}@example.com"
        user_id = f"user_{unique_id}_{i}"
        user = Node("User", id=user_id, name=f"User {user_id}", email=email)
        user.__primarylabel__ = "User"
        user.__primarykey__ = "id"
        users.append(user)

        graph.merge(user)

    # Create 3 post nodes
    posts = []
    for i in range(1, 4):
        post = Node("Post", id=i, title=f"Post {i}")
        post.__primarylabel__='Post'
        post.__primarykey__='title'
        posts.append(post)

        graph.merge(post)

    # Create random 'IS_FRIEND' relationships among users
    for user in users:
        # Choose random number of friends
        num_friends = random.randint(1, 6)
        # Ensure we don't choose the user themselves as a friend
        potential_friends = [u for u in users if u != user]
        for i in range(num_friends):
            friend = random.choice(potential_friends)
            # Make sure the friendship isn't already established
            rel = Relationship(user, "IS_FRIEND", friend)
            graph.merge(rel)
            # Add the inverse relationship as well, because friendship is mutual
            rel_inverse = Relationship(friend, "IS_FRIEND", user)
            graph.merge(rel_inverse)
            # Remove this user from potential friends so we don't pick them again
            potential_friends.remove(friend)

    # Create random 'LIKES' relationships between users and posts
    for user in users:
        # Each user likes a random number of posts
        num_likes = random.randint(1, 3)
        liked_posts = random.sample(posts, num_likes)
        for post in liked_posts:
            rel = Relationship(user, "LIKES", post)
            graph.merge(rel)

    return {
        'statusCode': 200,
        'body': 'Graph generation complete!'
    }
