import boto3
from py2neo import Graph
import random

def find_third_post(liked_posts):
    # Retrieve all post IDs
    all_post_ids = [1, 2, 3]  # Replace with your actual post IDs or retrieve from Neo4j

    # Find the third post that the user hasn't liked
    unliked_posts = list(set(all_post_ids) - set(liked_posts))

    if len(unliked_posts) > 0:
        return random.choice(unliked_posts)
    else:
        return None

def lambda_handler(event, context):
    # Connect to Neo4j - replace credentials and host with your own
    graph = Graph("bolt://ec2-3-83-254-136.compute-1.amazonaws.com:7687", auth=("neo4j", "A123456a"))

    # Execute Cypher query to retrieve user data with their liked posts
    query = """
    MATCH (u:User)-[:LIKES]->(p:Post)
    RETURN u, collect(p.id) AS liked_posts
    """

    result = graph.run(query)

    # Create an S3 client - replace 'YOUR_ACCESS_KEY' and 'YOUR_SECRET_KEY' with your own
    s3_client = boto3.client('s3', aws_access_key_id='AKIA4E7LK5EIH27ODFIL', aws_secret_access_key='+7ZaNCJ9/DBQdCdWtEIAKdt3IEkFKY8Xj5xyhJEo')

    # Bucket name and file name in S3
    bucket_name = 'devops-demo-bucket001'
    file_name = 'alert_messages.txt'

    # Retrieve the existing content of the file from the S3 bucket
    existing_content = ""
    try:
        existing_object = s3_client.get_object(Bucket=bucket_name, Key='DATA2/alertmessage.txt')
        existing_content = existing_object['Body'].read().decode('utf-8')
    except:
        pass

    # Loop through the result and generate alert messages for users who like two posts
    alert_messages = []
    for record in result:
        user = record['u']
        liked_posts = record['liked_posts']

        # Check if the user liked exactly two posts
        if len(liked_posts) == 2:
            third_post_id = find_third_post(liked_posts)

            if third_post_id is not None:
                # Generate alert message to send the user an ad about the third post
                message = f"Alert: Send ad about Post {third_post_id} to User {user['id']}"
                alert_messages.append(message)

    # Join the existing content, alert messages, and newlines
    messages_str = existing_content + '\n' + '\n'.join(alert_messages)

    # Upload the messages to the S3 bucket
    s3_client.put_object(Body=messages_str, Bucket=bucket_name, Key='DATA2/alertmessage.txt')

    return {
        'statusCode': 200,
        'body': f'Updated {len(alert_messages)} messages.'
    }
